#透過FB Javascript SDK client來授權取得使用者的粉絲專頁和粉專webhooks訂閱

## APP設定
#### 增加產品 **facebook Login**
 - 設定允許的網域 - facebook Login > config > 有效的 OAuth 重新導向 URI > 新增網頁domain
#### 商家驗證
#### 應用程式審查
- pages_messaging
- pages_manage_metadata


## fb登入按鍵
#### 產生按鍵
- https://developers.facebook.com/docs/facebook-login/web/login-button
#### 登入與取得授權
- 登入按鈕
```
<fb:login-button scope="pages_messaging,pages_manage_metadata" onlogin="testAPI();"></fb:login-button>

```

- 取得所有已授權的粉絲專頁id
```
<script>
    function testAPI() {

        FB.api('/me/accounts', function(response) {
            console.log('Successfully retrieved pages', response);
            var pages = response.data;
            for (var i = 0, len = pages.length; i < len; i++) {
                var page = pages[i];
                //console.log(page.id, page.access_token);
                page_subscribed_apps(page.id,page.access_token);


            }

        });
}
</script>
```
- 將所有已授權的粉絲專頁訂閱Webhooks(messages,messaging_postbacks)
```
<script>
function page_subscribed_apps(id,pageAccessToken) {
    //console.log(id,pageAccessToken);

    FB.api('/'+id+'/subscribed_apps?access_token='+pageAccessToken,'POST',{"subscribed_fields":"messages,messaging_postbacks"},
              function(response) {
                  console.log(id,response);
              }
            );

}
</script>
```


